#!/bin/sh

old_path="$PWD"
cd "$(dirname $(realpath "$0"))"

id=$(<config grep -oE 'id=.*' | sed -s 's/id=//')

if [ -z id ]; then
    echo "error: no id found"
    exit 1
fi

preserve_old_wallpapers=$(<config grep -oE 'preserve_old_wallpapers=.*' | sed -s 's/preserve_old_wallpapers=//')
use_wal=$(<config grep -oE 'use_wal=.*' | sed -s 's/use_wal=//')
notifications=$(<config grep -oE 'notifications=.*' | sed -s 's/notifications=//')

desktop=$XDG_CURRENT_DESKTOP
if [ -z $XDG_CURRENT_DESKTOP ]; then desktop=$XDG_SESSION_DESKTOP; fi

case "$desktop" in
    "GNOME")
        set_wallpaper() {
            gsettings set org.gnome.desktop.background picture-uri file:///"$1"
        }
        ;;
    "Cinnamon") # Untested
        set_wallpaper() {
            gsettings set org.cinnamon.desktop.background picture-uri file:///"$1"
        }
        ;;
    "KDE")
        set_wallpaper() {
            # Do nothing. The wallpaper type must be set to Slideshow with only the wallpapers folder added. KDE Plasma will take care of the rest.
            sleep 0
        }
        ;;
    "XFCE")
        set_wallpaper() {
            # Identical workaround to KDE
            sleep 0
        }
        ;;
    "LXDE")
        set_wallpaper() {
            pcmanfm -w "$1"
        }
        ;;
    "sway")
        set_wallpaper() {
            killall swaybg
            swaybg -i "$1" -m fit &
        }
        ;;
    *)
        set_wallpaper() {
            feh --bg-max "$1"
        }
        ;;
esac
poll() {
    response=$(curl -s "https://walltaker.joi.how/api/links/"$id".json" -0 --header "User-Agent: JBerliner-client")
    url=$(echo $response | grep -oE '"post_url":".*"' | sed -e 's/"post_url":"//' -e 's/".*//')
    set_by=$(echo $response | grep -oE '"set_by":".*"' | sed -e 's/"set_by":"//' -e 's/".*//')

    if [ -z "$set_by" ]; then set_by="Anonymous"; fi

    if [ "$url" != "$cache" ]; then
        cache=$url
        if [ $preserve_old_wallpapers = 0 ]; then rm wallpapers/*; fi
        filename=$(echo $url | sed -e 's/.*\///g')
        curl -O "$url"
        mv $filename wallpapers/$filename
        
        set_wallpaper $(realpath "wallpapers/$filename")

        if [ -n $use_wal ] && [ $use_wal -ne 0 ]; then
            wal -ni wallpapers/$filename  
        fi

        if [ -n $notifications ] && [ $notifications -ne 0 ]; then
            notify-send "Walltaker" "$(echo -e "New wallpaper set by $set_by \n")"
        fi
    fi
}

while true
do
    poll
    sleep 10
done

cd "$old_path"
