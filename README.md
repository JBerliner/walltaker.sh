# walltaker.sh

a "client" for [Walltaker](https://walltaker.joi.how)

## why does this exist

I, Joseph Berliner, got quite mad at the fact that the so-called Desktop Client for Walltaker does not support i3wm.
So I decided to slap a quick shell script together. And here it is.

## what does this do

there aren't many features. the thing polls the API endpoint for your link once every 10 seconds, and if the response is different from last time, it downloads the new image and changes your wallpaper.

## prerequisites

- `curl` (and an internet connection)
- `sed`
- `grep`
- `realpath`
- `sh`
- `mv`
- `echo`
- `feh` if you're using a WM or something else that does not provide its own wallpaper management utility

- `notify-send` (optionally)
- `wal` (optionally)

## what on earth is `wal`

`wal` (or `pywal`) is [this thing.](https://github.com/dylanaraps/pywal) It does a cool thing:
> Pywal is a tool that generates a color palette from the dominant colors in an image. It then applies the colors system-wide and on-the-fly in all of your favourite programs.

you can use it if you want to, there's a config option that enables it.

## officially supported DEs/WMs

if your DE/WM isn't supported please tell me and I might add support for it

- any WM (via `feh`)
- GNOME
- Cinnamon (untested)
- KDE Plasma (set wallpaper type to Slideshow, do not keep old wallpapers, and only include the folder where they are downloaded. use ~10s change rate for best results)
- XFCE (with an identical workaround to KDE)
- LXDE
- Sway (note: support not perfectly flawless because Wayland)

## installation

- `git clone https://gitlab.com/JBerliner/walltaker-client/`
- `cd walltaker-client`
- `cp config.dist config`
- edit `config` with your favourite text editor (mine is `sed`), fill in your link ID (remember that `true` and `false` don't exist here, use `1` and `0` instead!)
- `chmod +x walltaker.sh`
- pick your favourite way to run it at startup

## updates

- `git pull`

This should perform an update while maintaining your configuration.
